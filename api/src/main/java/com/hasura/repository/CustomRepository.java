package com.hasura.repository;

import com.hasura.model.FilmOfActor;
import com.hasura.model.FilmPerCategory;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class CustomRepository {

    private static final String FIND_CATEGORIES_FOR_FILM = "select categories.name from film_category fc\n" +
            "left outer join films on films.Id = fc.film_Id\n" +
            "left outer join categories on categories.Id = fc.category_id\n" +
            "where films.Id = :filmdId";

    private static final String FIND_FILMS_PER_CATEGORY = "select  films.title, films.Id from film_category fc\n" +
            "left outer join films on films.Id = fc.film_Id\n" +
            "left outer join categories on categories.Id = fc.category_id\n" +
            "where categories.Id = :categoryId";

    private static final String FIND_ACTORS_IN_FILM = "select actors.first_name, actors.last_name from film_actor fa\n" +
            "left outer join actors on actors.Id = fa.actor_id\n" +
            "left outer join films  on films.Id = fa.film_id\n" +
            "where films.id = :filmId";

    private static final String FIND_FILMS_OF_ACTOR = "select films.title, films.id from film_actor fa\n" +
            "left outer join actors on actors.Id = fa.actor_id\n" +
            "left outer join films  on films.Id = fa.film_id\n" +
            "where actors.id = :actorId";

    private final EntityManager entityManager;
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public List<String> findCategoriesForFilm(Integer filmId) {
        return entityManager.createNativeQuery(FIND_CATEGORIES_FOR_FILM)
                .setParameter("filmdId", filmId)
                .getResultList();
    }

    public List<FilmPerCategory> findFilmsOfCategory(Integer categoryId) {
        Map<String, Integer> params = new HashMap<>();
        params.put("categoryId", categoryId);
        return namedParameterJdbcTemplate.query(FIND_FILMS_PER_CATEGORY, params, (resultSet, i) ->
                new FilmPerCategory(resultSet.getObject("title", String.class), resultSet.getObject("Id", Integer.class))
        );
    }

    public List<String> findActorsInFilm(Integer filmId) {
        Map<String, Integer> params = new HashMap<>();
        params.put("filmId", filmId);
        return namedParameterJdbcTemplate.query(FIND_ACTORS_IN_FILM, params, (resultSet, i) ->
                String.format("%s %s", resultSet.getObject("first_name", String.class), resultSet.getObject("last_name", String.class))
        );
    }

    public List<FilmOfActor> findFilmsOfActor(Integer actorId) {
        Map<String, Integer> params = new HashMap<>();
        params.put("actorId", actorId);
        return namedParameterJdbcTemplate.query(FIND_FILMS_OF_ACTOR, params, (resultSet, i) ->
                new FilmOfActor(resultSet.getObject("title", String.class), resultSet.getObject("id", Integer.class))
        );
    }

}
