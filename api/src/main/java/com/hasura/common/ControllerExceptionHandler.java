package com.hasura.common;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {HasuraException.class})
    protected ResponseEntity<Object> handleHasuraException(HasuraException exception, WebRequest request) {
        String bodyOfErrorResponse = "Oops! An error occurred";
        return handleExceptionInternal(exception, bodyOfErrorResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
}
