package com.hasura.common;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class HasuraException extends RuntimeException{

    private final String exceptionMessage;

    @Override
    public String getMessage() {
        return exceptionMessage;
    }
}
