package com.hasura.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "categories")
public class Category {

    @Id
    @Column(name = "Id")
    private Integer id;

    @Column(name = "name")
    private String name;


}
