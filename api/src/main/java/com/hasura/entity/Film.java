package com.hasura.entity;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "films")
@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)
public class Film {

    @Id
    @Column(name = "Id")
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "release_year")
    private Integer releaseYear;

    @Column(name = "rating")
    private String rating;

    @Type(type = "list-array")
    @Column(name = "special_features")
    private List<String> specialFeatures;
}
