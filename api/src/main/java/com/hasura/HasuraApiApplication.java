package com.hasura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HasuraApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(HasuraApiApplication.class, args);
    }

}
