package com.hasura.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FilmPerCategory {
    private String filmName;
    private Integer filmId;
}
