package com.hasura.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilmOfActor {
    private String filmTitle;
    private Integer filmId;
}
