package com.hasura.controller;

import com.hasura.common.HasuraException;
import com.hasura.entity.Actor;
import com.hasura.entity.Film;
import com.hasura.model.FilmOfActor;
import com.hasura.model.FilmPerCategory;
import com.hasura.repository.ActorRepository;
import com.hasura.repository.CustomRepository;
import com.hasura.repository.FilmRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Validated
@CrossOrigin
@RequestMapping("/api")
public class GenericReadController {

    private final ActorRepository actorRepository;
    private final FilmRepository filmRepository;
    private final CustomRepository customRepository;

    @GetMapping("/actor/{id}")
    public ResponseEntity<Actor> getActorById(@Valid @NotNull @PathVariable Integer id) {
        Actor actor = actorRepository.findById(id).orElseThrow(() -> new HasuraException(String.format("Actor Not found for Id %s", id)));
        return new ResponseEntity<>(actor, HttpStatus.OK);
    }

    @GetMapping("/actor")
    public ResponseEntity<List<Actor>> getActors() {
        return new ResponseEntity<>(actorRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/film/{id}")
    public ResponseEntity<Film> getFilmById(@Valid @NotNull @PathVariable Integer id) {
        Film film = filmRepository.findById(id).orElseThrow(() -> new HasuraException(String.format("Film not found for Id %s", id)));
        return new ResponseEntity<>(film, HttpStatus.OK);
    }

    @GetMapping("/film")
    public ResponseEntity<List<Film>> getFilmB() {
        return new ResponseEntity<>(filmRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/films-of-category/{categoryId}")
    public ResponseEntity<List<FilmPerCategory>> getFilmsPerCategory(@Valid @NotNull @PathVariable Integer categoryId) {
        return new ResponseEntity<>(customRepository.findFilmsOfCategory(categoryId), HttpStatus.OK);
    }

    @GetMapping("/categories-for-film/{filmId}")
    public ResponseEntity<List<String>> getCategoriesForFilm(@Valid @NotNull @PathVariable Integer filmId) {
        return new ResponseEntity<>(customRepository.findCategoriesForFilm(filmId), HttpStatus.OK);
    }

    @GetMapping("/actors-in-film/{filmId}")
    public ResponseEntity<List<String>> getActorsInFilm(@Valid @NotNull @PathVariable Integer filmId) {
        return new ResponseEntity<>(customRepository.findActorsInFilm(filmId), HttpStatus.OK);
    }

    @GetMapping("/films-of-actor/{actorId}")
    public ResponseEntity<List<FilmOfActor>> getFilmsOfActor(@Valid @NotNull @PathVariable Integer actorId) {
        return new ResponseEntity<>(customRepository.findFilmsOfActor(actorId), HttpStatus.OK);
    }
}
