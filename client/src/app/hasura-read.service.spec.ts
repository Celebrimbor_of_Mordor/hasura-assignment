import { TestBed } from '@angular/core/testing';

import { HasuraReadService } from './hasura-read.service';

describe('HasuraReadService', () => {
  let service: HasuraReadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HasuraReadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
