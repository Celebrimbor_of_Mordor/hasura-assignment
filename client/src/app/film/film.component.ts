import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { HasuraReadService } from '../hasura-read.service';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {

  films: any[] = [];

  selectedFilm;
  showDialog = false;
  categoriesForFilm;
  actorsInFilm;

  constructor(
    private hasuraReadService: HasuraReadService
  ) { }

  ngOnInit(): void {
    this.hasuraReadService.getFilms().subscribe(
      (res: any[]) => this.films = res);
  }

  onRowSelectedEvent(event) {
    forkJoin({
      categoriesForFilm: this.hasuraReadService.getCategoriesForFilm(event.data.id),
      actorsInFilm: this.hasuraReadService.getActorsinFilm(event.data.id)
    }).subscribe(
      res => {
        this.categoriesForFilm = res.categoriesForFilm;
        this.actorsInFilm = res.actorsInFilm;
        this.showDialog = true;
      }
    )    
  }

}
