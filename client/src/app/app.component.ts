import { Component, OnInit } from '@angular/core';
import { HasuraReadService } from './hasura-read.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'hasura-client';
  constructor(
  ) {

  }

  ngOnInit() {
  }
}
