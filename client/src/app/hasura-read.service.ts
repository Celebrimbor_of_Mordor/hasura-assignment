import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Constants} from './constants';

@Injectable({
  providedIn: 'root'
})
export class HasuraReadService {

  

  constructor(
    private httpClient: HttpClient
  ) {}

  public getFilms() {
    let url = Constants.BASEURL +  '/film';
    return this.httpClient.get(url);
  }

  public getCategoriesForFilm(filmId: number) {
    return this.httpClient.get(Constants.BASEURL + `/categories-for-film/${filmId}`);
  }

  public getActorsinFilm(filmId: number) {
    return this.httpClient.get(Constants.BASEURL + `/actors-in-film/${filmId}`);
  }
}
